class PostPengumuman {
  PostPengumuman({
      this.judul,
      this.tanggal,
      this.deskripsi,
      this.photo_url,});

  PostPengumuman.fromJson(dynamic json) {
    judul = json['judul'];
    tanggal = json['tanggal'];
    deskripsi = json['deskripsi'];
    photo_url = json['photo_url'];
  }
  String? judul;
  String? tanggal;
  String? deskripsi;
  String? photo_url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['judul'] = judul;
    map['tanggal'] = tanggal;
    map['deskripsi'] = deskripsi;
    map['photo_url'] = photo_url;
    return map;
  }

}