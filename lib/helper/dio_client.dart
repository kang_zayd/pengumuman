import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:pengumuman/helper/http_response.dart';

class DioClient {
  late Dio _dio;

  Interceptor _interceptor() {
    return InterceptorsWrapper(onRequest: (RequestOptions request, RequestInterceptorHandler handler) async {
      request.headers.addAll({
        "Authorization": "Bearer yQYd16Ibmo9ELgzX-cpLgXHRz9GJXHjxFuyKcbWNrywCMmbtFg",
      });
      return handler.next(request);
    });
  }

  Future<HttpResponse> create({required String url, required Method method, Map<String, dynamic>? params, dynamic? json}) async {
    _dio = Dio();
    _dio.interceptors.add(_interceptor());
    _dio.interceptors.add(PrettyDioLogger(
        requestHeader: kDebugMode,
        requestBody: kDebugMode,
        responseBody: kDebugMode,
        responseHeader: false,
        error: kDebugMode,
        compact: kDebugMode,
        maxWidth: 90));
    Response response;

    try {
      if (method == Method.POST) {
        response = await _dio.post(url, data: params, options: Options(contentType: Headers.formUrlEncodedContentType));
      } else if (method == Method.POSTJSON) {
        response = await _dio.post(url, data: json, options: Options(contentType: Headers.jsonContentType));
      } else if (method == Method.DELETE) {
        response = await _dio.delete(url);
      } else if (method == Method.PATCH) {
        response = await _dio.patch(url, data: params);
      } else if (method == Method.PUT) {
        response = await _dio.put(url, data: params);
      } else {
        response = await _dio.get(url, queryParameters: params);
      }

      // handling respon message
      if (response.statusCode == 200 || response.statusCode == 201) {
        return HttpResponse(status: true, message: "${response.data['message']}", data: response.data);
        /*======================*/
      } else if (response.statusCode == 401) {
        return HttpResponse(status: false, message: "Unauthorized", data: response.data);
      } else if (response.statusCode == 500) {
        return HttpResponse(status: false, message: "Server Error", data: response.data);
      } else if (response.statusCode == 202) {
        return HttpResponse(status: false, message: "${response.data['message']}", data: response.data);
      } else {
        return HttpResponse(status: false, message: "Something does won't wrong", data: response.data);
      }
    } on SocketException {
      return HttpResponse(status: false, message: "Tidak ada koneksi internet");
    } on FormatException {
      return HttpResponse(status: false, message: "Bad response format");
    } on DioException catch (e) {
      return HttpResponse(status: false, message: "Error : ${e.message}", data: e.response, code: e.hashCode);
    } catch (e) {
      return HttpResponse(status: false, message: "Something won't wrong", data: e, code: e.hashCode);
    }
  }
}

enum Method { POST, POSTJSON, GET, PUT, DELETE, PATCH }
