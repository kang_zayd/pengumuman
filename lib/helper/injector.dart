import 'package:get_it/get_it.dart';
import 'package:pengumuman/helper/dio_client.dart';

final GetIt inject = GetIt.I;

Future<void> setupInjection() async {
  //Components
  inject.registerSingleton(DioClient());

  //ViewModels
  // inject.registerLazySingleton(() => DataDiriBloc(inject()));
}
