import 'package:pengumuman/helper/dio_client.dart';
import 'package:pengumuman/helper/http_response.dart';
import 'package:rxdart/rxdart.dart';
class DataDiriBloc {
  final DioClient _client;

  DataDiriBloc(this._client);

  final BehaviorSubject<ResponseState<List?>> _listDataDiri = BehaviorSubject();

  Stream<ResponseState<List?>> get listDataDiri => _listDataDiri.asBroadcastStream();

  final BehaviorSubject<ResponseState> _post = BehaviorSubject();

  Stream<ResponseState> get post => _post.asBroadcastStream();

  Future getData() async {
    _listDataDiri.add(ResponseState(status: Status.LOADING, message: 'Mengambil Data'));
    final response = await _client.create(url: "https://crudapi.co.uk/api/v1/pengumuman", method: Method.GET);
    ResponseState<List?> state;
    if (response.status) {
      state = ResponseState(
          status: Status.SUCCESS, message: response.message, data: response.data);
    } else {
      state = ResponseState(status: Status.ERROR, message: response.message, code: response.code);
    }
    _listDataDiri.add(state);
  }

  Future postData(param) async {
    _post.add(ResponseState(status: Status.LOADING, message: 'Mengirim Data'));
    ResponseState state;
    try {
      final response =
          await _client.create(url: "https://crudapi.co.uk/api/v1/pengumuman", method: Method.POSTJSON, json: param);
      if (response.status) {
        state = ResponseState(status: Status.SUCCESS, message: response.message);
      } else {
        state = ResponseState(status: Status.ERROR, message: response.message, code: response.code);
      }
    } catch (e) {
      state = ResponseState(status: Status.ERROR, message: e.toString());
    }
    _post.add(state);
    return state;
  }
}
