import 'package:flutter/material.dart';

class ListWithImage extends StatelessWidget {
  const ListWithImage({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: 3,
      itemBuilder: (ctx, index) {
        return Container(
          margin: const EdgeInsets.symmetric(vertical: 4, horizontal: 10),
          padding: const EdgeInsets.all(8.0),
          decoration: BoxDecoration(
            color: Colors.white,borderRadius: BorderRadius.all(Radius.circular(10))
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.network("https://www.smkn5solo.sch.id/wp-content/uploads/2023/04/TFQ9150.jpg"),
              SizedBox(height: 8),
              Text(
                "Judul ${index + 1}",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              Text(
                  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. ",
                  maxLines: 3),
              SizedBox(height: 10),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(Icons.account_circle_rounded, size: 14,),
                  Text("Admin",
                      style: TextStyle(color: Colors.black87, fontSize: 10, fontWeight: FontWeight.w400)),
                  SizedBox(width: 10),
                  Text("25-09-2023",
                      style: TextStyle(color: Colors.grey, fontSize: 10, fontWeight: FontWeight.w400)),
                ],
              )
            ],
          ),
        );
      }
    );
  }
}